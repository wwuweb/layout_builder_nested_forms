<?php

namespace Drupal\layout_builder_nested_forms;

use Drupal\Component\Utility\Html;
use Drupal\Core\Security\TrustedCallbackInterface;

final class FormElementConverter implements PostRendererInterface, TrustedCallbackInterface {

  public static function trustedCallbacks() {
    return ['postRender'];
  }

  public function postRender($content) {
    $html = Html::load($content);
    $forms = $html->getElementsByTagName('form');

    foreach ($forms as $form) {
      $replacement = $html->createElement('div');

      foreach ($form->childNodes as $child) {
        $replacement->appendChild($child->cloneNode(TRUE));
      }

      foreach ($form->attributes as $name => $attribute) {
        $replacement->setAttribute($name, $attribute->nodeValue);
      }

      $form->parentNode->replaceChild($replacement, $form);
    }

    return Html::serialize($html);
  }

}
