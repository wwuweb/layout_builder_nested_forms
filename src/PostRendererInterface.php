<?php

namespace Drupal\layout_builder_nested_forms;

interface PostRendererInterface {

  public function postRender($content);

}
