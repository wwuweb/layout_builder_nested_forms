<?php

namespace Drupal\layout_builder_nested_forms\EventSubscriber;

use Drupal\layout_builder_nested_forms\PostRendererInterface;

use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class BlockComponentRenderArray implements EventSubscriberInterface {

  private $postRenderer;

  public function __construct(PostRendererInterface $post_renderer) {
    $this->postRenderer = $post_renderer;
  }

  public static function getSubscribedEvents() {
    return [
      LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY => ['onBuildRender'],
    ];
  }

  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    if ($event->inPreview()) {
      $build = $event->getBuild();
      $build['#post_render'][] = [$this->postRenderer, 'postRender'];
      $event->setBuild($build);
    }
  }

}
